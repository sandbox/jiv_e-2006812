$(document).ready(function(){
  var helps = $('.description, .help');

  if(helps.length) {
    var showtext = Drupal.t('Show help');
    var hidetext = Drupal.t('Hide help');

    if($.cookie('Drupal.visitor.help_toggle_sandbox_hide')) {
      helps.hide();
      var help_is_visible = false;
    }
    else {
      var help_is_visible = true;
    }

    $('.help_toggle_sandbox a').css('cursor','pointer').click(function(event) {
      //Prevent the page refresh
      event.preventDefault();
      var text = help_is_visible ? showtext : hidetext;
      $('.help_toggle_sandbox a').text(text);
      if(help_is_visible) {
        helps.slideUp('fast');
        $.cookie('Drupal.visitor.help_toggle_sandbox_hide', 'on', { expires: 365, path: '/' });
      }
      else {
        helps.slideDown('fast');
        $.removeCookie('Drupal.visitor.help_toggle_sandbox_hide', { path: '/' });
      }
      help_is_visible = !help_is_visible;
    });
  }
});
