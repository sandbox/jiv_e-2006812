CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Is this module needed?
 * Installation
 * Help toggle
 * Help toggle UI
 * Some known problems

INTRODUCTION
------------
Current Maintainer: jiv_e juho.viitasalo -at- validi.fi

Help toggle adds a 'Hide help'/'Show help' link on pages that contain help text
or field descriptions for hiding and showing them.

Drupal 7 version is coming!

IS THIS NEEDED?
---------------
Yes.
- https://drupal.org/node/476332
- http://drupal.stackexchange.com/questions/18756/drupaliest-way-to-remove-login-and-registration-page-descriptions
- http://drupal.stackexchange.com/questions/18644/removing-input-formatting-options-and-description-from-comment-form
https://drupal.org/node/476332

No.
- https://drupal.org/node/476332#comment-2884078

Still... this module hides and shows help texts in different way than suggested
tooltip approach does. Help toggle is simple. Just enable it and it works. It
toggles all the help texts at the same time. This makes reading of them easier.
No need to hover or click to open popups.  When help texts are not needed they
won't get in your way. Only one clearly worded link needs to be shown on the
top of the page. Even your grandma can use it. It also works without javascript.

INSTALLATION
------------
1. Copy the module folder to your preferred contrib module folder eg.
   sites/all/modules.

2. Copy https://raw.github.com/carhartl/jquery-cookie/master/jquery.cookie.js to
   sites/all/libraries/jquery.cookie. Make sure to create libraries and
   jquery.cookie folders first.

3. Enable the module/modules.

HELP TOGGLE
-----------
This module requires no configuration. Just enable it
and you can start toggling your help texts.

HELP TOGGLE UI
--------------
This module adds an settings page for the Help
toggle module. It can be found from admin/settings/help_toggle_sandbox. On this
page you can make some advanced settings like disabling javascript, filter the
pages where you want the toggle link to appear and disabling help and
description texts entirely.

After you have made your settings you can disable the UI module. This way it
won't clutter your admin navigation.

SOME KNOWN PROBLEMS
-------------------
If the javascript is not enabled in your
browser or you have selected not to use javascript on the modules settings page
and if the current page contains forms they will be cleared when the toggle link
is clicked. I don't see any way to fix this because toggling without javascript
demands page refresh.
